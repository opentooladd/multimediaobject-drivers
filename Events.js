import PubSub from "pubsub.js"
import serialize from "./libs/microutils/dist/functions/serialize"
import unserialize from "./libs/microutils/dist/functions/unserialize"

export const createTopic = (namespace, topicName) => {
  if (namespace.topics[topicName]) return
  namespace.topics[topicName] = {
    name: topicName,
    subscribers: [],
  }
}

export const subscribe = function (namespace, topicName, fn, options) {
  if (typeof fn !== "function") throw new TypeError(`Cannot subscribe to ${topicName}. Provide a Function`)
  if (!namespace.topics[topicName]) createTopic(namespace, topicName)
  const token =  PubSub.subscribe(namespace.topics[topicName].name, fn, options)
  namespace.topics[topicName].subscribers.push({
    token,
    fn,
    options,
  })
  return token
}

export const unsubscribe = function (namespace, token) {
  Object.values(namespace.topics).find(t => {
    const sb = t.subscribers.find(s => s.token === token)
    if (sb) {
      const sbIndex = t.subscribers.findIndex(s => s.token === token)
      if (sbIndex >= 0) t.subscribers.splice(sbIndex, 1)
      return sb
    } else {
      return
    }
  })
  return PubSub.unsubscribe(token)
}

export const publish = function (namespace, topicName, ...args) {
  return PubSub.publish(topicName, [...args])
}

/**
 * 
 * Driver update method
 * trigger the update cycle
 * 
 * @param {Object} namespace 
 * @param {Object} mo 
 */
export const update = function update(namespace, ...args) {
  if (namespace.$hooks) namespace.$hooks.filter(h => h.type === "update").forEach(h => h.fn.apply(this, [namespace, ...args]))
}

const exportToJSON = ({
  topics,
  $hooks,
}) => ({
  exportedTopics: Object.keys(topics).reduce((acc, key) => {
    const topic = {
      name: key,
      subscribers: topics[key].subscribers.map(m => serialize(m.fn)),
    }
    acc.push(topic)
    return acc
  }, []),
  exportedHooks: $hooks.map(({ type, fn }) => ({
    type,
    fn: serialize(fn),
  })),
})

export class EventsDriver {
  constructor(namespace) {
    this.id = "Events"
    this.namespace = namespace
    this.namespace.topics = this.namespace.topics || {}
    this.namespace.$hooks = this.namespace.$hooks || [] 
    this.namespace.$hooks = this.namespace.$hooks.concat(namespace.exportedHooks ? namespace.exportedHooks.map(({ type, fn }) => ({
      type,
      fn: unserialize(fn),
    })) : [])
    delete this.namespace.exportedHooks
    if (this.namespace.exportedTopics) {
      this.namespace.exportedTopics.forEach((topic) => {
        this.createTopic(topic.name)
        topic.subscribers.forEach(sub => this.subscribe(topic.name, unserialize(sub)))
      })
      delete this.namespace.exportedTopics
    }
  }
  createTopic(topicName) { return createTopic.call(this, this.namespace, topicName) }
  subscribe(topicName, fn, options) { return subscribe.call(this, this.namespace, topicName, fn, options) }
  unsubscribe(token) { return unsubscribe.call(this, this.namespace, token) }
  publish(topicName, ...args) { return publish.call(this, this.namespace, topicName, ...args) }
  update(mo) { return update.call(this, this.namespace, mo) }
  exportToJSON() { return exportToJSON.call(this, this.namespace) }
}
