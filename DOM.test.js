/**
 * @jest-environment jsdom
 */

import { DOMDriver as DOMDriverConstructor } from "./DOM"

let DOM_NAMESPACE = {}
let DOMDriver

describe("DOMDriver", () => {
  beforeEach(() => {
    DOM_NAMESPACE = {}
    DOMDriver = new DOMDriverConstructor(DOM_NAMESPACE)
  })

  afterEach(() => {
    while(document.body.children.length) {
      document.body.removeChild(document.body.children[document.body.children.length === 0 ? 0 : document.body.children.length - 1])
    }
  })
  test("should have an id of DOM", () => {
    expect(DOMDriver.id).toEqual("DOM")
  })

  test("hooks", () => {
    const hookMock = jest.fn()
    const nonHook = jest.fn()
    
    const hooks = {
      $hooks: [
        {
          type: "update",
          fn: nonHook,
        },
      ]
    }
    const DDriver = new DOMDriverConstructor(hooks)
    expect(nonHook).not.toHaveBeenCalled()

    DDriver.update()
    expect(nonHook).toHaveBeenCalledWith(DDriver.namespace)
    expect(nonHook.mock.contexts[0]).toEqual(DDriver)
  })

  describe(".update", () => {
    test("should launch", () => {
      const originalUpdate = DOMDriver.update
      DOMDriver.update = jest.fn(DOMDriver.update)
      DOMDriver.update(DOM_NAMESPACE)
      expect(DOMDriver.update).toHaveBeenCalledWith(DOM_NAMESPACE)
      DOMDriver.update = originalUpdate
    })
  })

  test(".createDOMElement", () => {
    expect(DOMDriver.createDOMElement()).toBeTruthy()
  })

  test(".removeDOMElement", () => {
    DOMDriver.createDOMElement()
    DOMDriver.appendDOMElement()
    expect(DOM_NAMESPACE).toBeTruthy()
    
    expect(document.body.children).toHaveLength(1)
    
    DOMDriver.removeDOMElement()

    expect(document.body.children).toHaveLength(0)
  })

  test(".bindDOMEvents", () => {
    const oConsole = console
    console.error = jest.fn(console.error)

    DOMDriver.createDOMElement()
    DOMDriver.appendDOMElement()

    DOM_NAMESPACE.element.addEventListener = jest.fn(DOM_NAMESPACE.element.addEventListener)
    DOM_NAMESPACE.element.removeEventListener = jest.fn(DOM_NAMESPACE.element.removeEventListener)

    const events = {
      click: jest.fn(),
    }

    DOMDriver.bindDOMEvents(events)

    expect(DOM_NAMESPACE.element.addEventListener).toHaveBeenCalledWith("click", events.click)
    expect(DOM_NAMESPACE.element.removeEventListener).toHaveBeenCalledWith("click", events.click)

    expect(() => DOMDriver.bindDOMEvents({ click: "test" })).not.toThrow()
    expect(console.error).toHaveBeenCalledWith("events.click is not a function")

    console.error = oConsole.error
  })

  test(".appendDOMElement", () => {
    const el = document.createElement("div")
    const el2 = document.createElement("div")
    const el3 = DOMDriver.createDOMElement()
    DOMDriver.appendDOMElement(el)
    
    expect(el.children).toHaveLength(1)
    
    DOMDriver.appendDOMElement(el2)

    expect(el.children).toHaveLength(0)
    expect(el2.children).toHaveLength(1)

    DOMDriver.appendDOMElement(el2)

    expect(el2.children).toHaveLength(1)
  })

  test(".setAttributes", () => {
    const attributes = {
      "data-index": 10,
      "data-object-id": "testestsatetest",
      textContent: "test de texte"
    }
    const element = DOMDriver.createDOMElement("div")

    DOMDriver.setAttributes(attributes)

    expect(element.getAttribute("data-index")).toEqual(attributes["data-index"].toString())
    expect(element.getAttribute("data-object-id")).toEqual(attributes["data-object-id"])
    expect(element.getAttribute("textContent")).toEqual(null)
    expect(element.textContent).toEqual(attributes.textContent)
    expect(() => DOMDriver.setAttributes([])).toThrowError(/should be of type/)
  })

  test(".setAttributes with default namespace", () => {
    DOM_NAMESPACE.attributes = {
      "data-index": 10,
      "data-object-id": "testestsatetest",
      textContent: "test de texte"
    }
    const element = DOMDriver.createDOMElement("div")

    DOMDriver.setAttributes()

    expect(DOMDriver.getAttribute("data-index")).toEqual(DOM_NAMESPACE.attributes["data-index"].toString())
    expect(DOMDriver.getAttribute("data-object-id")).toEqual(DOM_NAMESPACE.attributes["data-object-id"])
    expect(DOMDriver.getAttribute("textContent")).toEqual(DOM_NAMESPACE.attributes.textContent)
    expect(element.textContent).toEqual(DOM_NAMESPACE.attributes.textContent)
  })

  test(".getAttributes with default namespace", () => {
    DOM_NAMESPACE.attributes = {
      "data-index": 10,
      "data-object-id": "testestsatetest",
      textContent: "test de texte"
    }
    DOMDriver.createDOMElement("div")
    DOMDriver.setAttributes()

    const response = DOMDriver.getAttributes()

    expect(response["data-index"]).toEqual(DOM_NAMESPACE.attributes["data-index"].toString())
    expect(response["data-object-id"]).toEqual(DOM_NAMESPACE.attributes["data-object-id"])
    expect(response["textContent"]).toEqual(DOM_NAMESPACE.attributes.textContent)
    expect(response.textContent).toEqual(DOM_NAMESPACE.attributes.textContent)
  })

  test(".removeAttributes", () => {
    const attributes = {
      "data-index": 10,
      "data-object-id": "testestsatetest",
    }
    const element = DOMDriver.createDOMElement("div")

    DOMDriver.setAttributes(attributes)
    DOMDriver.removeAttributes(Object.keys(attributes))

    expect(element.getAttribute("data-index")).toBeNull()
    expect(element.getAttribute("data-object-id")).toBeNull()
    expect(() => DOMDriver.removeAttributes({})).toThrowError(/should be of type/)
  })

  test(".exportToJSON", () => {
    const hooks = [
      {
        type: "init",
        fn: function initHook(namespace, mo) {
          namespace.test = true
        }
      } 
    ]
    const driver = new DOMDriverConstructor({
      $hooks: hooks,
      attributes: {
        "data-mo-id": "test",
        id: "test",
      },
      nodeName: "div",
    })
    
    const json = driver.exportToJSON()

    expect(json).toEqual({
      exportedHooks: [{
        type: "init",
        fn: {
          args: ["namespace"," mo"],
          body: `
        namespace.test = true;
      `,
        }
      }], 
      attributes: {
        "data-mo-id": "test",
        id: "test",
      },
      nodeName: "div", 
    })

    const driver2 = new DOMDriverConstructor({
      $hooks: [
        {
          type: "update",
          fn: function updateHook(namespace, mo) {
            namespace.testUpdate = true
          }
        },
      ],
      ...json,
    })
    expect(driver2.namespace.$hooks).toHaveLength(2)
    expect(driver2.namespace.test).not.toBe(true)
    driver2.update()
    expect(driver2.namespace.testUpdate).toBe(true)
  })
})
