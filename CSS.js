import getNumFromString from "./libs/microutils/dist/numbers/getNumFromString"
import getUnitFromString from "./libs/microutils/dist/numbers/getUnitFromString"
import uuid from "./libs/microutils/dist/lang/uuid"
import serialize from "./libs/microutils/dist/functions/serialize"
import unserialize from "./libs/microutils/dist/functions/unserialize"

import check from "./libs/check/dist/index"

export const transformProperties = [
  "translate",
  "translateX",
  "translateY",
  "translateZ",
  "translateX",
  "translate-x",
  "translateY",
  "translate-y",
  "translate-z",
  "scale",
  "scaleX",
  "scaleY",
  "scaleZ",
  "scale-x",
  "scale-y",
  "scale-z",
  "rotate",
  "rotateX",
  "rotateY",
  "rotateZ",
  "rotate-x",
  "rotate-y",
  "rotate-z",
  "skew",
  "skewX",
  "skewY",
  "skewZ",
  "skew-x",
  "skew-y",
  "skew-z",
]

/**
 * transform a trasnform value (in CSS) in a suitable form
 * to be treated afterward.
 * 
 * @param {string} k 
 * @param {string | Number} v
 * 
 * @return Object{string, string, number} - {string, unit, value} 
 */
export const transformValueForProperty = (k, v) => {
  let value = 0
  let unit
  value = typeof v === "string" && v.indexOf(",") >= 0
    ? v.split(",").map(getNumFromString)
    : getNumFromString(v)
  unit = typeof v === "string" && v.indexOf(",") >= 0
    ? v.split(",").map(getUnitFromString)
    : getUnitFromString(v)

  let string = `${k}(${value}${unit})`
  if (value instanceof Array && unit instanceof Array) {
    let res = ""
    value.forEach((val, index) => {
      res += index > 0
        ? `, ${val}${unit[index]}`
        : `${val}${unit[index]}`
    })
    string = `${k}(${res})`
  }
  return { string, unit, value }
}


/**
 * Transform the 3d transform properties in a CSS driver namespace.
 * Add a 'transform' propertie to the namespace that represents the correctly
 * ordonnanced properties.
 * 
 * @param {Object} namespace - appropriate namespace for the driver
 */
export const applyTransformProperties = (namespace) => {
  const transforms = Object.keys(namespace.style).filter(k => transformProperties.includes(k))
    .reduce((acc, k) => {
      acc.push([k, namespace.style[k]])
      return acc
    }, [])
  let z = [0, 1, 2, 3]
  const trans = {
    x: namespace.style.translateX ? getNumFromString(namespace.style.translateX) : 0,
    y: namespace.style.translateY ? getNumFromString(namespace.style.translateY) : 0,
    z: namespace.style.translateZ ? getNumFromString(namespace.style.translateZ) : 0,
    xU: namespace.style.translateX ? getUnitFromString(namespace.style.translateX) : "px",
    yU: namespace.style.translateY ? getUnitFromString(namespace.style.translateY) : "px",
    zU: namespace.style.translateZ ? getUnitFromString(namespace.style.translateZ) : "px",
  }
  const rot = {
    x: namespace.style.rotateX ? getNumFromString(namespace.style.rotateX) : 0,
    y: namespace.style.rotateY ? getNumFromString(namespace.style.rotateY) : 0,
    z: namespace.style.rotateZ ? getNumFromString(namespace.style.rotateZ) : 0,
    u: "deg",
  }
  const ske = {
    x: namespace.style.skewX ? getNumFromString(namespace.style.skewX) : 0,
    y: namespace.style.skewY ? getNumFromString(namespace.style.skewY) : 0,
    u: "deg",
  }
  const sca = {
    x: namespace.style.scaleX ? getNumFromString(namespace.style.scaleX) : 1,
    y: namespace.style.scaleY ? getNumFromString(namespace.style.scaleY) : 1,
  }

  if (transforms.length > 0) {
    const v = (transforms.map(transform => transformValueForProperty(transform[0], transform[1]).string))

    v.forEach((a) => {
      if (a.indexOf("translateX") >= 0 || a.indexOf("translate-x") >= 0) {
        trans.x = getNumFromString(a)
        trans.xU = getUnitFromString(a)
      } else if (a.indexOf("translateY") >= 0 || a.indexOf("translate-y") >= 0) {
        trans.y = getNumFromString(a)
        trans.yU = getUnitFromString(a)
      } else if (a.indexOf("translateZ") >= 0 || a.indexOf("translate-z") >= 0) {
        trans.z = getNumFromString(a)
        trans.zU = getUnitFromString(a)
      }

      if (a.indexOf("rotateX") >= 0 || a.indexOf("rotate-x") >= 0) {
        rot.x = getNumFromString(a)
      } else if (a.indexOf("rotateY") >= 0 || a.indexOf("rotate-y") >= 0) {
        rot.y = getNumFromString(a)
      } else if (a.indexOf("rotateZ") >= 0 || a.indexOf("rotate-z") >= 0) {
        rot.z = getNumFromString(a)
      } else if (a.indexOf("rotate") >= 0) {
        const sp = a.split(",")
        rot.x = getNumFromString(sp[0])
        rot.y = getNumFromString(sp[1])
        rot.z = getNumFromString(sp[2])
      }

      if (a.indexOf("scaleX") >= 0 || a.indexOf("scale-x") >= 0) {
        sca.x = getNumFromString(a)
      } else if (a.indexOf("scaleY") >= 0 || a.indexOf("scale-y") >= 0) {
        sca.y = getNumFromString(a)
      }
      if (a.indexOf("skewX") >= 0 || a.indexOf("skew-x") >= 0) {
        ske.x = getNumFromString(a)
      } else if (a.indexOf("skewY") >= 0 || a.indexOf("skew-y") >= 0) {
        ske.y = getNumFromString(a)
      }
    })

    z[0] = `translate3d(${trans.x}${trans.xU},${trans.y}${trans.yU},${trans.z}${trans.zU})`
    z[1] = `rotateX(${rot.x}${rot.u}) rotateY(${rot.y}${rot.u}) rotateZ(${rot.z}${rot.u})`
    z[2] = `skew(${ske.x}${ske.u},${ske.y}${ske.u})`
    z[3] = `scale(${sca.x},${sca.y})`

    namespace.style["transform"] = z.join(" ")
  }
}

/**
 * applyStyle apply all the properties to the driver's namespace
 * 
 * @param {object} namespace 
 * @param {object} properties
 */
export const applyStyle = (namespace, properties = namespace.style) => {
  check.check(properties, Object)
  const keys = Object.keys(properties)
  keys.forEach(k => namespace.style[k] = properties[k])
  applyTransformProperties(namespace)
  
  return namespace.style
}

/**
 * toStyleString returns a style string for the DOM
 * 
 * @param {object} namespace 
 */
export const toStyleString = (namespace) => {
  applyStyle(namespace)
  const styleString = Object.keys(namespace.style).reduce((acc, val) => {
    acc += `${val}: ${namespace.style[val]};`
    return acc
  }, "")
  namespace.styleString = styleString
  
  return styleString
}

/**
  * Add a global style object to the page
  * @param {object} namespace - driver's namespace
  * @param {function} callback - the callback to execute
  * @returns {Promise}
  */

export const globalStyle = (namespace) => {
  applyStyle(namespace, namespace.style)
  namespace.global.id = namespace.global.id || uuid()
  const str = `.${namespace.global.id} {
    ${Object.keys(namespace.style).reduce((prev, key) => {
    return `${prev}
    ${key}: ${namespace.style[key]};` 
  }, "").trim()}
}`
  return str
}

/**
 * 
 * Driver update method
 * trigger the update cycle
 * 
 * @param {Object} namespace 
 * @param {Object} mo 
 */
export const update = function update(namespace, mo)  {
  if (namespace.$hooks) namespace.$hooks.filter(h => h.type === "update").forEach(h => h.fn.apply(this, [mo]))
}

const exportToJSON = ({
  style,
  global,
  $hooks,
}) => ({
  style,
  global,
  exportedHooks: $hooks.map(({ type, fn }) => ({
    type,
    fn: serialize(fn),
  })),
})

export class CSSDriver {
  constructor(namespace) {
    this.id = "CSS"
    this.namespace = namespace || {}
    this.namespace.style = this.namespace.style || {}
    this.namespace.global = this.namespace.global || { style: "" }
    this.namespace.$hooks = this.namespace.$hooks || [] 
    this.namespace.$hooks = this.namespace.$hooks.concat(this.namespace.exportedHooks ? this.namespace.exportedHooks.map(({ type, fn }) => ({
      type,
      fn: unserialize(fn),
    })) : [])
    delete this.namespace.exportedHooks
  }
  update(mo) { return update.call(this, this.namespace, mo) }
  applyStyle(style) { return applyStyle.call(this, this.namespace, style) }
  toStyleString() { return toStyleString.call(this, this.namespace) }
  globalStyle() { return globalStyle.call(this, this.namespace) }
  exportToJSON() { return exportToJSON.call(this, this.namespace) }
}
