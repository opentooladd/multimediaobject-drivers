/**
 * @jest-environment jsdom
 */

import { AnimationDriver } from "./Animation"

describe("drivers/Animation", () => {
  let driver = null
  let NAMESPACE = null

  let target = null

  beforeEach(() => {
    target = {}
    NAMESPACE = {
      targets: [target],
    }

    driver = new AnimationDriver(NAMESPACE)
  })

  test("hooks", () => {
    const hookMock = jest.fn()
    const nonHook = jest.fn()
    
    const hooks = {
      $hooks: [
        {
          type: "update",
          fn: nonHook,
        },
      ],
    }
    const ADriver = new AnimationDriver(hooks)
    expect(nonHook).not.toHaveBeenCalled()

    ADriver.update()
    expect(nonHook).toHaveBeenCalled()
    expect(nonHook.mock.contexts[0]).toEqual(ADriver)
  })

  test("should start all values at default", () => {
    const otherNamespace = {}
    const d = new AnimationDriver(otherNamespace)
    expect(otherNamespace.currentAnimationName).toBeDefined()
    expect(otherNamespace.currentAnimation).toBeDefined()
    expect(otherNamespace.currentAnimation).toBeDefined()
    expect(otherNamespace.totalTime).toBeDefined()
    expect(otherNamespace.progress).toEqual(0)
    expect(otherNamespace.targets).toBeDefined()
    expect(otherNamespace.instance).not.toBeDefined()
    expect(otherNamespace.timeline).not.toBeDefined()
  })

  test(".updateInstance should create a new instance with the currentAnimation", () => {
    const animation = {
      translateX: [
        { value: 100, duration: 1200 },
        { value: 0, duration: 800 },
      ],
    }

    NAMESPACE.animations = animation
    driver.updateInstance()

    expect(NAMESPACE.instance.animatables).toHaveLength(1)
  })

  test(".updateInstance should instanciate a timeline if there are childs", () => {
    const animation = {
      translateX: [
        { value: 100, duration: 1200 },
      ],
    }
    let c
    c = {
      targets: {
        translateX: 0,
      },
      currentAnimation: {
        translateX: [
          { value: 100, duration: 1200 },
          { value: 0, duration: 800 },
        ],
      },
      MOParent: NAMESPACE,
      childs: [
        {
          targets: {
            translateX: 0,
          },
          MOParent: c,
          currentAnimation: {
            translateX: [
              { value: 100, duration: 1200 },
              { value: 0, duration: 800 },
            ],
          },
        },
      ],
    }
    const childs = [
      c,
      c,
    ]

    NAMESPACE.targets = {
      translateX: 0,
    }
    NAMESPACE.currentAnimation = animation
    NAMESPACE.childs = childs
    driver.updateInstance()

    expect(NAMESPACE.timeline.duration).toEqual(2000)
    expect(NAMESPACE.timeline.children).toHaveLength(5)
    const child1Instance = Object.assign({}, NAMESPACE.childs[0].instance)
    const child2Instance = Object.assign({}, NAMESPACE.childs[1].instance)
    const child1fChild0 = Object.assign({}, NAMESPACE.childs[0].childs[0].instance)
    const child3Instance = Object.assign({}, NAMESPACE.childs[0].childs[0].instance)
    delete NAMESPACE.timeline.id
    delete child1Instance.id
    delete child2Instance.id
    delete child1fChild0.id
    delete child3Instance.id
    expect(JSON.stringify(NAMESPACE.timeline)).toEqual(JSON.stringify(child1Instance))
    expect(JSON.stringify(child1Instance)).toEqual(JSON.stringify(child2Instance))
    expect(JSON.stringify(child1fChild0)).toEqual(JSON.stringify(child3Instance))

    expect(NAMESPACE.timeline.children[0].animations[0].tweens).toHaveLength(1)
    NAMESPACE.timeline.children.slice(1).forEach(ch => ch.animations.forEach(a => expect(a.tweens).toHaveLength(2)))
  })


  test(".updateInstance should instanciate a timeline if there are childs and clean correctly", () => {
    const animation = {
      translateX: [
        { value: 100, duration: 1200 },
      ],
    }
    let c
    c = {
      targets: {
        translateX: 0,
      },
      currentAnimation: {
        translateX: [
          { value: 100, duration: 1200 },
          { value: 0, duration: 800 },
        ],
      },
      MOParent: NAMESPACE,
      childs: [
        {
          targets: {
            translateX: 0,
          },
          MOParent: c,
          currentAnimation: {
            translateX: [
              { value: 100, duration: 1200 },
              { value: 0, duration: 800 },
            ],
          },
        },
      ],
    }
    const childs = [
      c,
      c,
    ]

    NAMESPACE.targets = {
      translateX: 0,
    }
    NAMESPACE.currentAnimation = animation
    NAMESPACE.childs = childs
    driver.updateInstance()

    expect(NAMESPACE.timeline.duration).toEqual(2000)
    expect(NAMESPACE.timeline.children).toHaveLength(5)

    driver.updateInstance()

    expect(NAMESPACE.timeline.duration).toEqual(2000)
    expect(NAMESPACE.timeline.children).toHaveLength(5)
  })

  test("should apply animation hooks if they are any", async () => {
    const mo = {
      id: "test_mo"
    }

    const mock_complete = jest.fn()
    const mock_begin = jest.fn()
    const mock_loopComplete = jest.fn()
    const mock_loopBegin = jest.fn()
    const mock_change = jest.fn()
    const mock_changeBegin = jest.fn()
    const mock_changeComplete = jest.fn()
    const mock_update = jest.fn()
    const animationHooks = {
      complete: mock_complete,
      begin: mock_begin,
      loopComplete: mock_loopComplete,
      loopBegin: mock_loopBegin,
      change: mock_change,
      changeBegin: mock_changeBegin,
      changeComplete: mock_changeComplete,
      update: mock_update,
    }
    const animation = {
      translateX: [
        { value: 100, duration: 100 },
        { value: 0, duration: 200 },
      ],
    }

    NAMESPACE.currentAnimation = animation
    driver.namespace = {
      ...NAMESPACE,
      ...animationHooks,
      mo,
    }
    driver.updateInstance()


    driver.play()
    await driver.finished()
    expect(mock_complete.mock.contexts[0]).toEqual(mo)
    expect(mock_complete).toHaveBeenCalled()
    expect(mock_begin.mock.contexts[0]).toEqual(mo)
    expect(mock_begin).toHaveBeenCalled()
    expect(mock_loopComplete.mock.contexts[0]).toEqual(mo)
    expect(mock_loopComplete).toHaveBeenCalled()
    expect(mock_loopBegin.mock.contexts[0]).toEqual(mo)
    expect(mock_loopBegin).toHaveBeenCalled()
    expect(mock_change.mock.contexts[0]).toEqual(mo)
    expect(mock_change).toHaveBeenCalled()
    expect(mock_changeBegin.mock.contexts[0]).toEqual(mo)
    expect(mock_changeBegin).toHaveBeenCalled()
    expect(mock_changeComplete.mock.contexts[0]).toEqual(mo)
    expect(mock_changeComplete).toHaveBeenCalled()
    expect(mock_update.mock.contexts[0]).toEqual(mo)
    expect(mock_update).toHaveBeenCalled()
  })


  test("should apply animation hooks with childs if they are any", async () => {
    const mo = {
      id: "test_mo"
    }

    const mock_complete = jest.fn()
    const mock_begin = jest.fn()
    const mock_loopComplete = jest.fn()
    const mock_loopBegin = jest.fn()
    const mock_change = jest.fn()
    const mock_changeBegin = jest.fn()
    const mock_changeComplete = jest.fn()
    const mock_update = jest.fn()
    const animationHooks = {
      complete: mock_complete,
      begin: mock_begin,
      loopComplete: mock_loopComplete,
      loopBegin: mock_loopBegin,
      change: mock_change,
      changeBegin: mock_changeBegin,
      changeComplete: mock_changeComplete,
      update: mock_update,
    }
    const animation = {
      translateX: [
        { value: 100, duration: 100 },
        { value: 0, duration: 200 },
      ],
    }

    NAMESPACE.currentAnimation = animation
    NAMESPACE.childs = [
      {
        ...animation,
        currentAnimation: animation,
      },
      {
        ...animation,
        currentAnimation: animation,
      }
    ]
    driver.namespace = {
      ...NAMESPACE,
      ...animationHooks,
      mo,
    }
    driver.updateInstance()


    driver.play()
    await driver.finished()
    expect(mock_complete.mock.contexts[0]).toEqual(mo)
    expect(mock_complete).toHaveBeenCalled()
    expect(mock_begin.mock.contexts[0]).toEqual(mo)
    expect(mock_begin).toHaveBeenCalled()
    expect(mock_loopComplete.mock.contexts[0]).toEqual(mo)
    expect(mock_loopComplete).toHaveBeenCalled()
    expect(mock_loopBegin.mock.contexts[0]).toEqual(mo)
    expect(mock_loopBegin).toHaveBeenCalled()
    expect(mock_change.mock.contexts[0]).toEqual(mo)
    expect(mock_change).toHaveBeenCalled()
    expect(mock_changeBegin.mock.contexts[0]).toEqual(mo)
    expect(mock_changeBegin).toHaveBeenCalled()
    expect(mock_changeComplete.mock.contexts[0]).toEqual(mo)
    expect(mock_changeComplete).toHaveBeenCalled()
    expect(mock_update.mock.contexts[0]).toEqual(mo)
    expect(mock_update).toHaveBeenCalled()
  })

  test(".play should play the animation", async () => {
    const animation = {
      translateX: [
        { value: 100, duration: 100 },
        { value: 0, duration: 200 },
      ],
    }

    NAMESPACE.currentAnimation = animation
    driver.updateInstance()

    driver.play()
    await driver.finished()
    expect(NAMESPACE.instance.began).toEqual(true)
  })
  test(".pause should pause the animation", async () => {
    const animation = {
      translateX: [
        { value: 100, duration: 100 },
        { value: 0, duration: 200 },
      ],
    }

    NAMESPACE.currentAnimation = animation
    driver.updateInstance()

    driver.play()
    await driver.finished()
    driver.pause()
    expect(NAMESPACE.instance.began).toEqual(true)
    expect(NAMESPACE.instance.paused).toEqual(true)
  })

  test(".restart should restart the animation", async () => {
    const animation = {
      translateX: [
        { value: 100, duration: 100 },
        { value: 0, duration: 200 },
      ],
    }

    NAMESPACE.currentAnimation = animation
    driver.updateInstance()

    driver.play()
    await driver.finished()
    driver.pause()
    expect(NAMESPACE.instance.began).toEqual(true)
    expect(NAMESPACE.instance.paused).toEqual(true)

    driver.restart()
  })

  test(".reverse should reverse the animation", async () => {
    const animation = {
      translateX: [
        { value: 100, duration: 10 },
      ],
    }

    NAMESPACE.currentAnimation = animation
    driver.updateInstance()

    driver.play()
    await driver.finished()
    driver.pause()
    expect(NAMESPACE.instance.began).toEqual(true)
    expect(NAMESPACE.instance.paused).toEqual(true)

    driver.reverse()

    expect(NAMESPACE.instance.direction).toEqual("reverse")
  })

  test(".seek should seek the animation", () => {
    const animation = {
      translateX: [
        { value: 100, duration: 1000 },
      ],
    }

    NAMESPACE.currentAnimation = animation
    driver.updateInstance()

    driver.seek(500)

    expect(NAMESPACE.instance.progress).toEqual(50)
    expect(NAMESPACE.instance.currentTime).toEqual(500)
  })

  test(".update should update the animation", () => {
    const animation = {
      translateX: [
        { value: 100, duration: 1000 },
      ],
    }

    NAMESPACE.currentAnimation = animation
    driver.updateInstance()

    driver.update()
  })

  test(".easings should return an easing list", () => {
    const easingProperties = ["easeInQuad",
      "easeInCubic",
      "easeInQuart",
      "easeInQuint",
      "easeInSine",
      "easeInExpo",
      "easeInCirc",
      "easeInBack",
      "easeOutQuad",
      "easeOutCubic",
      "easeOutQuart",
      "easeOutQuint",
      "easeOutSine",
      "easeOutExpo",
      "easeOutCirc",
      "easeOutBack",
      "easeInBounce",
      "easeInOutQuad",
      "easeInOutCubic",
      "easeInOutQuart",
      "easeInOutQuint",
      "easeInOutSine",
      "easeInOutExpo",
      "easeInOutCirc",
      "easeInOutBack",
      "easeInOutBounce",
      "easeOutBounce",
      "easeOutInQuad",
      "easeOutInCubic",
      "easeOutInQuart",
      "easeOutInQuint",
      "easeOutInSine",
      "easeOutInExpo",
      "easeOutInCirc",
      "easeOutInBack",
      "easeOutInBounce",
      "cubicBezier",
      "spring",
      "steps",
      "custom"
    ]

    for (const key of easingProperties) {
      expect(driver.namespace.easings).toContain(key)
    }
  })

  test(".changeAnimation should change the currentAnimation if it exists", () => {
    const animations = {
      test: {
        translateX: [
          { value: 100, duration: 1000 },
        ],
      },
    }

    NAMESPACE.animations = animations

    expect(() => driver.changeAnimation("test")).not.toThrow()
    expect(NAMESPACE.currentAnimation).toEqual(animations.test)
    expect(NAMESPACE.currentAnimationName).toEqual("test")

    expect(() => driver.changeAnimation("lol")).toThrow(Error("animation lol doesn't exist"))
  })

  test(".deleteAnimation should an animation and change to default if it was selected", () => {
    const animations = {
      default: {
        width: [
          { value: "10%", duration: 1000 },
        ],
      },
      test: {
        translateX: [
          { value: 100, duration: 1000 },
        ],
      },
    }

    NAMESPACE.animations = animations

    driver.changeAnimation("test")

    expect(() => driver.deleteAnimation("test")).not.toThrow()
    expect(NAMESPACE.animations.test).toBeUndefined()
    expect(NAMESPACE.currentAnimation).toEqual(animations.default)
  })

  test(".exportToJSON", () => {
    const hooks = [
      {
        type: "init",
        fn: function initHook(mo) {
          this.namespace.test = true
        }
      } 
    ]
    const driver = new AnimationDriver({
      $hooks: hooks,
      animations: {
        default: {
          translateX: [
            { value: 100, duration: 100 },
            { value: 0, duration: 200 },
          ],
        }
      },
    })
    
    const json = driver.exportToJSON()

    expect(json).toEqual({
      exportedHooks: [{
        type: "init",
        fn: {
          args: ["mo"],
          body: `
        this.namespace.test = true;
      `,
        }
      }], 
      totalTime: 10,
      animations: {
        default: {
          translateX: [
            { value: 100, duration: 100 },
            { value: 0, duration: 200 },
          ],
        }
      },
      currentAnimation: {
        translateX: [
          { value: 100, duration: 100 },
          { value: 0, duration: 200 },
        ],
      },
      currentAnimationName: "default",
    })

    const driver2 = new AnimationDriver({
      $hooks: [
        {
          type: "update",
          fn: function updateHook(mo) {
            this.namespace.testUpdate = true
          }
        },
      ],
      ...json,
    })
    expect(driver2.namespace.$hooks).toHaveLength(2)
    expect(driver2.namespace.test).not.toBe(true)
    driver2.update({})
    expect(driver2.namespace.testUpdate).toBe(true)
  })
})
