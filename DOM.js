import check from "./libs/check/dist/index"
import serialize from "./libs/microutils/dist/functions/serialize"
import unserialize from "./libs/microutils/dist/functions/unserialize"

const DOMNodeProperties = [
  "innerText",
  "textContent",
  "value"
]

/**
 * 
 * create a DOM element based on {nodeName}
 * 
 * @param {string} nodeName 
 * @return {DOMElement} element
 */
export const createDOMElement = function createDOMElement(namespace, nodeName = "div") {
  const element = document.createElement(nodeName)
  namespace.element = element
  return element
}

/**
 * 
 * create a DOM element based on {type}
 * 
 * @param {Object} namespace - the Driver namespace
 */
export const removeDOMElement = function removeDOMElement(namespace) {
  if (namespace.element.parentElement) namespace.element.parentElement.removeChild(namespace.element)
}

/**
 * 
 * append a DOM element to a parent or body
 * 
 * @param {Object} namespace - the Driver namespace
 * @param {DOMElement} parent - the DOMElement parent
 */
export const appendDOMElement = function appendDOMElement(namespace, parent) {
  if (namespace.element.parentElement !== parent && namespace.element.parentElement) removeDOMElement(namespace)
  if (!namespace.element.parentElement && parent !== namespace.element) {
    const root = parent || document.body
    root.appendChild(namespace.element)
  }
}


/**
 * 
 * set element's attributes based on attributes
 * 
 * @param {Object} namespace - the Driver namespace
 * @param {Object} attributes - attributes Object structure { attrtName: value }
 * @return {DOMElement} element
 */
export const setAttributes = function setAttributes(namespace, attributes = namespace.attributes) {
  check.check(attributes, Object)
  for (let key in attributes) {
    if (!DOMNodeProperties.includes(key)) namespace.element.setAttribute(key, attributes[key])
    else namespace.element[key] = attributes[key]
  }

  return namespace.element
}

/**
 * 
 * get element's attributes based on attributes
 * 
 * @param {Object} namespace - the Driver namespace
 * @param {Object} attributes - attributes Object structure { attrtName: value }
 * @return {Object} response
 */
export const getAttributes = function getAttributes(namespace, attributes = namespace.attributes) {
  check.check(attributes, Object)
  const response = {}
  for (let key in attributes) {
    if (!DOMNodeProperties.includes(key)) {
      response[key] = namespace.element.getAttribute(key)
    } else {
      response[key] = namespace.element[key]
    }
  }

  return response
}

export const getAttribute = function getAttribute(namespace, attribute) {
  if (!DOMNodeProperties.includes(attribute)) {
    return namespace.element.getAttribute(attribute)
  } else {
    return namespace.element[attribute]
  }
}

export const bindDOMEvents = function bindDOMEvents(namespace, events) {
  for (const eventName in events) {
    if (typeof events[eventName] === "function") {
      namespace.element.removeEventListener(eventName, events[eventName])
      namespace.element.addEventListener(eventName, events[eventName])
    } else {
      console.error(`events.${eventName} is not a function`)
    }
  }
}

export const setDOMParent = function setDOMParent(namespace, DOMParent) {
  namespace.DOMParent = DOMParent
}

/**
 * 
 * remove element's attributes based on attributes
 * 
 * @param {Object} namespace - the Driver namespace
 * @param {Object} attributes - attributes Object structure { attrtName: value }
 * @return {DOMElement} element
 */
export const removeAttributes = function removeAttributes(namespace, attributes) {
  check.check(attributes, Array)
  attributes.forEach(attr => namespace.element.removeAttribute(attr))
  return namespace.element
}

/**
 * 
 * Driver update method
 * trigger the update cycle
 * 
 * @param {Object} namespace 
 * @param {Object} mo 
 */
export const update = function update(namespace, ...args) {
  if (namespace.$hooks) namespace.$hooks.filter(h => h.type === "update").forEach(h => h.fn.apply(this, [namespace, ...args]))
}

export const init = function init(namespace) {
  namespace.element = createDOMElement(namespace, namespace.nodeName)
  setDOMParent(namespace, namespace.element.parentElement || namespace.DOMParent)
  namespace.attributes = namespace.attributes || {}
  setAttributes(namespace, namespace.attributes, namespace.element)
}

const exportToJSON = ({
  nodeName,
  attributes,
  $hooks,
}) => ({
  nodeName,
  attributes,
  exportedHooks: $hooks.map(({ type, fn }) => ({
    type,
    fn: serialize(fn),
  })),
})

export class DOMDriver {
  constructor(namespace) {
    this.id = "DOM"
    this.namespace = namespace || {}
    this.namespace.$hooks = namespace.$hooks || [] 
    this.namespace.$hooks = this.namespace.$hooks.concat(namespace.exportedHooks ? namespace.exportedHooks.map(({ type, fn }) => ({
      type,
      fn: unserialize(fn),
    })) : [])
    delete this.namespace.exportedHooks
    this.init()
  }

  setDOMParent(...args) { return setDOMParent.call(this, this.namespace, ...args) }
  update(...args) { return update.call(this, this.namespace, ...args) }
  init(...args) { return init.call(this, this.namespace, ...args) }
  createDOMElement(...args) { return createDOMElement.call(this, this.namespace, ...args) }
  removeDOMElement(...args) { return removeDOMElement.call(this, this.namespace, ...args) }
  appendDOMElement(...args) { return appendDOMElement.call(this, this.namespace, ...args) }
  setAttributes(...args) { return setAttributes.call(this, this.namespace, ...args) }
  getAttributes() { return getAttributes.call(this, this.namespace) }
  getAttribute(attribute) { return getAttribute.call(this, this.namespace, attribute) }
  removeAttributes(...args) { return removeAttributes.call(this, this.namespace, ...args) }
  bindDOMEvents(...args) { return bindDOMEvents.call(this, this.namespace, ...args) }
  exportToJSON() { return exportToJSON.call(this, this.namespace) }
}
