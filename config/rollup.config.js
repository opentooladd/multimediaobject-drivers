import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { globbySync } from 'globby';
import terser from '@rollup/plugin-terser';

const baseConfigs = (min = false) => globbySync('*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  input: inputFile,
  output: {
    name: `${inputFile.substring(0, inputFile.lastIndexOf("."))}Driver`,
    file: min ? `dist/${inputFile.replace('.js', '.min.js')}` : `dist/${inputFile}`,
    format: 'es',
  },
  plugins: [
    nodeResolve(),
    commonjs({
      include: 'node_modules/**',
    }),
    ...[(min ? terser() : undefined)]
  ],
}));

const umdConfigs = (min = false) => globbySync('*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  input: inputFile,
  output: {
    name: `${inputFile.substring(0, inputFile.lastIndexOf("."))}Driver`,
    file: min ? `dist/${inputFile.replace('.js', '.umd.min.js')}` : `dist/${inputFile.replace('.js', '.umd.js')}`,
    format: 'umd',
  },
  plugins: [
    nodeResolve(),
    commonjs({
      include: 'node_modules/**',
    }),
    ...[(min ? terser() : undefined)]
  ],
}));

const iifeConfigs = (min = false) => globbySync('*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  input: inputFile,
  output: {
    name: `${inputFile.substring(0, inputFile.lastIndexOf("."))}Driver`,
    file: min ? `dist/${inputFile.replace('.js', '.iife.min.js')}` : `dist/${inputFile.replace('.js', '.iife.js')}`,
    format: 'iife',
  },
  plugins: [
    nodeResolve(),
    commonjs({
      include: 'node_modules/**',
    }),
    ...[(min ? terser() : undefined)]
  ],
}));

const cjsConfigs = (min = false) => globbySync('*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  input: inputFile,
  output: {
    name: `${inputFile.substring(0, inputFile.lastIndexOf("."))}Driver`,
    file: min ? `dist/${inputFile.replace('.js', '.cjs.min.js')}` : `dist/${inputFile.replace('.js', '.cjs.js')}`,
    format: 'cjs',
  },
  plugins: [
    nodeResolve(),
    commonjs({
      include: 'node_modules/**',
    }),
    ...[(min ? terser() : undefined)]
  ],
}));

export default [
  ...baseConfigs(),
  ...umdConfigs(),
  ...iifeConfigs(),
  ...cjsConfigs(),
  // min config
  ...baseConfigs(true),
  ...umdConfigs(true),
  ...iifeConfigs(true),
  ...cjsConfigs(true)
];