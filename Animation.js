import serialize from "./libs/microutils/dist/functions/serialize"
import unserialize from "./libs/microutils/dist/functions/unserialize"

import anime from "animejs"

const config = ({
  totalTime: 10,
})

const progress = (namespace, anim) => {
  namespace.progress = anim.progress
}

const addChildToTimeline = (namespace, timeline) => {
  timeline.add({
    targets: namespace.targets,
    ...namespace.currentAnimation,
  }, namespace.currentAnimation.offset || 0)
  namespace.instance = timeline
  if (namespace.childs) namespace.childs.forEach((c) => addChildToTimeline(c, timeline))
}

const updateInstance = (namespace) => {
  if (namespace.childs.length > 0 && !namespace.MOParent) {
    namespace.timeline = anime.timeline({
      loop: namespace.loop || false,
      autoplay: namespace.autoplay || false,
      complete: namespace.complete ? namespace.complete.bind(namespace.mo) : null,
      begin: namespace.begin ? namespace.begin.bind(namespace.mo) : null,
      loopComplete: namespace.loopComplete ? namespace.loopComplete.bind(namespace.mo) : null,
      loopBegin: namespace.loopBegin ? namespace.loopBegin.bind(namespace.mo) : null,
      change: namespace.change ? namespace.change.bind(namespace.mo) : null,
      changeBegin: namespace.changeBegin ? namespace.changeBegin.bind(namespace.mo) : null,
      changeComplete: namespace.changeComplete ? namespace.changeComplete.bind(namespace.mo) : null,
      update: namespace.update ? namespace.update.bind(namespace.mo) : progress.bind(undefined, namespace),
    })
    namespace.timeline.add({
      targets: namespace.targets,
      ...namespace.currentAnimation,
    })
    namespace.timeline.getValue = anime.getValue
    namespace.childs.forEach((c) => addChildToTimeline(c, namespace.timeline))
  } else if (namespace.childs.length === 0 && !namespace.MOParent) {
    namespace.instance = anime({
      targets: namespace.targets,
      loop: namespace.loop || false,
      autoplay: namespace.autoplay || false,
      ...namespace.currentAnimation,
      complete: namespace.complete ? namespace.complete.bind(namespace.mo) : null,
      begin: namespace.begin ? namespace.begin.bind(namespace.mo) : null,
      loopComplete: namespace.loopComplete ? namespace.loopComplete.bind(namespace.mo) : null,
      loopBegin: namespace.loopBegin ? namespace.loopBegin.bind(namespace.mo) : null,
      change: namespace.change ? namespace.change.bind(namespace.mo) : null,
      changeBegin: namespace.changeBegin ? namespace.changeBegin.bind(namespace.mo) : null,
      changeComplete: namespace.changeComplete ? namespace.changeComplete.bind(namespace.mo) : null,
      update: namespace.update ? namespace.update.bind(namespace.mo) : progress.bind(undefined, namespace),
    })
    namespace.instance.getValue = anime.getValue
  }
}

const deleteAnimation = (namespace, animationName) => {
  delete namespace.animations[animationName]
  if (namespace.currentAnimationName === animationName) {
    namespace.currentAnimation = namespace.animations.default
    namespace.currentAnimationName = "default"
  }
}

const changeAnimation = (namespace, animationName) => {
  if (!namespace.animations[animationName]) throw new Error(`animation ${animationName} doesn't exist`)
  namespace.currentAnimationName = animationName
  namespace.currentAnimation = namespace.animations[animationName]
}

const play = namespace => namespace.timeline ? namespace.timeline.play() : namespace.instance.play()
const pause = namespace => namespace.timeline ? namespace.timeline.pause() : namespace.instance.pause()
const restart = namespace => namespace.timeline ? namespace.timeline.restart() : namespace.instance.restart()
const reverse = namespace => namespace.timeline ? namespace.timeline.reverse() : namespace.instance.reverse()
const seek = (namespace, time) => namespace.timeline ? namespace.timeline.seek(time) : namespace.instance.seek(time)
const finished = namespace => namespace.timeline ? namespace.timeline.finished : namespace.instance.finished

/**
 * 
 * Driver update method
 * trigger the update cycle
 * 
 * @param {Object} namespace 
 * @param {Object} mo 
 */
export const update = function update(namespace, ...args) {
  if (namespace.$hooks) namespace.$hooks.filter(h => h.type === "update").forEach(h => h.fn.apply(this, [namespace, ...args]))
}

export const init = function init(namespace) {
  namespace.animations = namespace.animations || {}
  namespace.currentAnimationName = namespace.currentAnimationName || "default"
  namespace.currentAnimation = namespace.currentAnimation || namespace.animations[namespace.currentAnimationName] || {}
  namespace.totalTime = namespace.totalTime || config.totalTime
  namespace.progress = 0
  namespace.targets = namespace.targets || []
  namespace.autoplay = namespace.autoplay || false

  namespace.easings = [
    "easeInQuad",
    "easeInCubic",
    "easeInQuart",
    "easeInQuint",
    "easeInSine",
    "easeInExpo",
    "easeInCirc",
    "easeInBack",
    "easeOutQuad",
    "easeOutCubic",
    "easeOutQuart",
    "easeOutQuint",
    "easeOutSine",
    "easeOutExpo",
    "easeOutCirc",
    "easeOutBack",
    "easeInBounce",
    "easeInOutQuad",
    "easeInOutCubic",
    "easeInOutQuart",
    "easeInOutQuint",
    "easeInOutSine",
    "easeInOutExpo",
    "easeInOutCirc",
    "easeInOutBack",
    "easeInOutBounce",
    "easeOutBounce",
    "easeOutInQuad",
    "easeOutInCubic",
    "easeOutInQuart",
    "easeOutInQuint",
    "easeOutInSine",
    "easeOutInExpo",
    "easeOutInCirc",
    "easeOutInBack",
    "easeOutInBounce",
    "cubicBezier",
    "spring",
    "steps",
    "custom"
  ]

  namespace.childs = namespace.childs || []
}

const exportToJSON = ({
  animations,
  currentAnimationName,
  currentAnimation,
  totalTime,
  $hooks,
}) => ({
  animations,
  currentAnimationName,
  currentAnimation,
  totalTime,
  exportedHooks: $hooks.map(({ type, fn }) => ({
    type,
    fn: serialize(fn),
  })),

})

export class AnimationDriver {
  constructor(namespace) {
    this.id = "Animation"
    this.namespace = namespace
    this.namespace.$hooks = this.namespace.$hooks || [] 
    this.namespace.$hooks = this.namespace.$hooks.concat(this.namespace.exportedHooks ? this.namespace.exportedHooks.map(({ type, fn }) => ({
      type,
      fn: unserialize(fn),
    })) : [])
    delete this.namespace.exportedHooks
    this.init()
  }

  init(...args) { return init.call(this, this.namespace, ...args) }
  updateInstance() { return updateInstance.call(this, this.namespace) }
  addChildToTimeline(timeline) { return addChildToTimeline.call(this, this.namespace, timeline) }
  changeAnimation(animationName) { return changeAnimation.call(this, this.namespace, animationName) }
  deleteAnimation(animationName) { return deleteAnimation.call(this, this.namespace, animationName) }
  update(mo) { return update.call(this, this.namespace, mo) }
  play() { return play.call(this, this.namespace) }
  pause() { return pause.call(this, this.namespace) }
  restart() { return restart.call(this, this.namespace) }
  reverse() { return reverse.call(this, this.namespace) }
  seek(time) { return seek.call(this, this.namespace, time) }
  finished() { return finished.call(this, this.namespace) }
  exportToJSON() { return exportToJSON.call(this, this.namespace) }
}
export { progress, updateInstance, play, pause, restart, reverse, seek, finished }
