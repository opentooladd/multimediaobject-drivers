'use strict';

var difference = (source, inputs) => {
  const flatArray = inputs.filter(i => (i !== null && typeof i !== 'undefined')).map(i => i instanceof Object ? JSON.stringify(i) : i);
  let diffs = source
  .filter(i => (i !== null && typeof i !== 'undefined'))
  .filter((i) => {
    const e = i instanceof Object ? JSON.stringify(i) : i;
    const index = flatArray.indexOf(e);
    if (index >= 0) flatArray.splice(index, 1);
    return !(index >= 0);
  });
  return diffs;
};

const nRegEx = new RegExp(/[\-\+]{0,1}\d+(\.\d+)?/, 'g');
const maxRegEx = new RegExp(/^>\d+(\.\d+)?$/);
const minRegEx = new RegExp(/^<\d+(\.\d+)?$/);
const rangeRegEx = new RegExp(/^\d+(\.\d+)?<>\d+(\.\d+)?$/);

const isNotNativeFunction = (input) => {
  return !input.toString().includes('function Function() { [native code] }')
        && input !== Array
        && input !== Function
        && input !== Object
        && input !== String
        && input !== Number;
};

var index = ({
  validate(input, schema, strict = false) {
    const keys = Object.keys(schema);
    const notVarKeys = keys.filter(k => !k.includes('?'));
    const inputKeys = Object.keys(input);
    const keyDifference = difference(notVarKeys, inputKeys);

    if (keyDifference.length > 0) return this.logError({
      input,
      source: keys,
      valid: false,
      message: `ValidationError: ${JSON.stringify(input)} missing keys [${keyDifference}]`
    });

    inputKeys.forEach(key => {
      const source = (schema[key] || schema[`?${key}`]);
      if (source) this.check(input[key], source);
      else if (!source && strict) return this.checkArray(inputKeys, keys);
    });

    return null;
  },
  check(input, source) {
    if (typeof source !== 'undefined' && source !== null) {
      if (source instanceof Function) {
        if (isNotNativeFunction(source)) {
          const result = source(input);
          return this.logError({
            input,
            source,
            valid: result,
            message: `ValidationError: ${JSON.stringify(input)} did not pass ${source.toString()}: returned ${result}`,
          });
        }
        return this.checkType(input, source);
      }
      const numberTest = (maxRegEx.test(source) || minRegEx.test(source) || rangeRegEx.test(source)) || this.getType(source) === 'Number';
      const stringTest = typeof source === 'string' || source instanceof RegExp || this.getType(source) === 'String';
      const arrayTest = source instanceof Array;

      if (numberTest) {
        return this.checkNumber(input, source);
      } else if (stringTest) {
        return this.checkString(input, source);
      } else if (arrayTest) {
        const typeArray = source.filter(o => o instanceof Function);
        if (typeArray.length > 0 && typeArray.length === source.length) {
          let success = false;
          for (let i = 0; i < typeArray.length; i++) {
            try {
              success = true;
              this.checkType(input, typeArray[i]);
              break;
            } catch (e) {
              success = false;
            }
          }
          if (!success) {
            return this.logError({
              input,
              source,
              valid: false,
              message: `${input} should be of type ${typeArray.map(a => this.getType(a))}`
            });
          }
          return null;
        } else {
          return this.checkArray(input, source);
        }
      } else {
        return this.checkObject(input, source);
      }
    }
    return null;
  },
  checkType(input, source) {
    const t = input ? `${(this.getType(source)[0]).toLowerCase()}${this.getType(source).slice(1)}` : true;
    const tInput = input ? `${(input.constructor.name[0]).toLowerCase()}${input.constructor.name.slice(1)}` : false;
    if (t !== tInput) {
      return this.logError({
        input,
        source,
        valid: false,
        message: `${JSON.stringify(input)} should be of type ${this.getType(source)}`
      });
    }
    return null;
  },
  checkString(input, source) {
    if (source instanceof RegExp) {
      return this.logError({
        input,
        source,
        valid: source.test(input),
      });
    } else {
      return this.logError({
        input,
        source,
        valid: source === input,
      });
    }
  },
  checkNumber(input, source) {
    const minTest = minRegEx.test(source);
    const maxTest = maxRegEx.test(source);
    const rangeTest = rangeRegEx.test(source);
    if (rangeTest && !minTest && !maxTest) {
      const matchMin = source.match(nRegEx)[0];
      const min = parseFloat(matchMin);
      const matchMax = source.match(nRegEx)[1];
      const max = parseFloat(matchMax);
      return this.logError({
        input,
        source,
        valid: input < max && input > min,
      });
    } else if (maxTest && !minTest && !rangeTest) {
      const match = source.match(nRegEx)[0];
      const n = parseFloat(match);
      return this.logError({
        input,
        source,
        valid: input > n,
      });
    } else {
      const match = source.match(nRegEx)[0];
      const n = parseFloat(match);
      return this.logError({
        input,
        source,
        valid: input !== '' && typeof input !== 'undefined' && input !== null && input < n,
      });
    }
  },
  checkArray(input, source) {
    const diffs = difference(source, input);
    return this.logError({
      input: JSON.stringify(input),
      source: JSON.stringify(source),
      valid: diffs.length === 0,
    });
  },
  checkObject(input, source) {
    return this.validate(input, source);
  },
  logError(error) {
    const message = `ValidationError: ${error.input} type:${typeof error.input} must respect ${error.source} type:${typeof error.source}`;
    if (!error.valid) {
      throw new TypeError(error.message || message);
    }
    return null;
  },
  getType(source) {
    return source.name || typeof source;
  }
});

var check = index;

const COMMENT_REG_EXP$1 = new RegExp(/\n+|(\/\*\*\/\n)+/, 'g');
const NEW_LINE_REG_EXP$1 = new RegExp(/^(\n+|\t+|\t\n+)(?!\w)$/, 'gm');
const BACKTICK_REG_EXP$1 = new RegExp(/`/, 'gm');

const serialize = (funct) => {
  if (!(funct instanceof Function)) throw new TypeError('serialize: argument should be a function');
  const txt = funct.toString();
  const args = txt.slice(txt.indexOf('(') + 1, txt.indexOf(')')).split(',');
  const body = txt.slice(txt.indexOf('{') + 1, txt.lastIndexOf('}'));
  return {
    args: args.map(el => el.replace(COMMENT_REG_EXP$1, '')
      .replace(NEW_LINE_REG_EXP$1, '')
      .replace(BACKTICK_REG_EXP$1, '')),
    body,
  };
};

var serialize$1 = serialize;

const COMMENT_REG_EXP = new RegExp(/\n+|(\/\*\*\/\n)+/, 'g');
const NEW_LINE_REG_EXP = new RegExp(/^(\n+|\t+|\t\n+)(?!\w)$/, 'gm');
const BACKTICK_REG_EXP = new RegExp(/`/, 'gm');
const COMMENTS_AND_BACKTICK = new RegExp(/(\/\*``\*\/)+/, 'g');

var unserialize = (serialized) => {
  const args = serialized.args.map(el => el.replace(COMMENT_REG_EXP, '').replace(COMMENTS_AND_BACKTICK, '').replace(NEW_LINE_REG_EXP, '').replace(BACKTICK_REG_EXP, ''));
  const { body } = serialized;
  let func = new Function();
  try {
    func = new Function(args, body);
  } catch (e) {
    console.error(e);
  }
  return func;
};

var unserialize$1 = unserialize;

const DOMNodeProperties = [
  'innerText',
  'textContent',
  'value'
];

/**
 * 
 * create a DOM element based on {nodeName}
 * 
 * @param {string} nodeName 
 * @return {DOMElement} element
 */
const createDOMElement = function createDOMElement(namespace, nodeName = 'div') {
  const element = document.createElement(nodeName);
  namespace.element = element;
  return element;
};

/**
 * 
 * create a DOM element based on {type}
 * 
 * @param {Object} namespace - the Driver namespace
 */
const removeDOMElement = function removeDOMElement(namespace) {
  if (namespace.element.parentElement) namespace.element.parentElement.removeChild(namespace.element);
};

/**
 * 
 * append a DOM element to a parent or body
 * 
 * @param {Object} namespace - the Driver namespace
 * @param {DOMElement} parent - the DOMElement parent
 */
const appendDOMElement = function appendDOMElement(namespace, parent) {
  if (namespace.element.parentElement !== parent && namespace.element.parentElement) removeDOMElement(namespace);
  if (!namespace.element.parentElement && parent !== namespace.element) {
    const root = parent || document.body;
    root.appendChild(namespace.element);
  }};


/**
 * 
 * set element's attributes based on attributes
 * 
 * @param {Object} namespace - the Driver namespace
 * @param {Object} attributes - attributes Object structure { attrtName: value }
 * @return {DOMElement} element
 */
const setAttributes = function setAttributes(namespace, attributes = namespace.attributes) {
  check.check(attributes, Object);
  for (let key in attributes) {
    if (!DOMNodeProperties.includes(key)) namespace.element.setAttribute(key, attributes[key]);
    else namespace.element[key] = attributes[key];
  }

  return namespace.element;
};

/**
 * 
 * get element's attributes based on attributes
 * 
 * @param {Object} namespace - the Driver namespace
 * @param {Object} attributes - attributes Object structure { attrtName: value }
 * @return {Object} response
 */
const getAttributes = function getAttributes(namespace, attributes = namespace.attributes) {
  check.check(attributes, Object);
  const response = {};
  for (let key in attributes) {
    if (!DOMNodeProperties.includes(key)) {
      response[key] = namespace.element.getAttribute(key);
    } else {
      response[key] = namespace.element[key];
    }
  }

  return response;
};

const getAttribute = function getAttribute(namespace, attribute) {
  if (!DOMNodeProperties.includes(attribute)) {
    return namespace.element.getAttribute(attribute);
  } else {
    return namespace.element[attribute];
  }
};

const bindDOMEvents = function bindDOMEvents(namespace, events) {
  for (const eventName in events) {
    if (typeof events[eventName] === 'function') {
      namespace.element.removeEventListener(eventName, events[eventName]);
      namespace.element.addEventListener(eventName, events[eventName]);
    } else {
      console.error(`events.${eventName} is not a function`);
    }
  }
};

const setDOMParent = function setDOMParent(namespace, DOMParent) {
  namespace.DOMParent = DOMParent;
};

/**
 * 
 * remove element's attributes based on attributes
 * 
 * @param {Object} namespace - the Driver namespace
 * @param {Object} attributes - attributes Object structure { attrtName: value }
 * @return {DOMElement} element
 */
const removeAttributes = function removeAttributes(namespace, attributes) {
  check.check(attributes, Array);
  attributes.forEach(attr => namespace.element.removeAttribute(attr));
  return namespace.element;
};

/**
 * 
 * Driver update method
 * trigger the update cycle
 * 
 * @param {Object} namespace 
 * @param {Object} mo 
 */
const update = function update(namespace, ...args) {
  const parent = this;
  if (namespace.$hooks) namespace.$hooks.filter(h => h.type === 'update').forEach(h => h.fn.apply(parent, [namespace, ...args]));
};

const init = function init(namespace, ...args) {
  namespace.element = createDOMElement(namespace, namespace.nodeName);
  setDOMParent(namespace, namespace.element.parentElement || namespace.DOMParent);
  namespace.attributes = namespace.attributes || {};
  setAttributes(namespace, namespace.attributes, namespace.element);
};

const exportToJSON = ({
  nodeName,
  attributes,
  $hooks,
}) => ({
  nodeName,
  attributes,
  exportedHooks: $hooks.map(({ type, fn }) => ({
    type,
    fn: serialize$1(fn),
  })),
});

class DOMDriver {
  constructor(namespace) {
    this.id = 'DOM';
    this.namespace = namespace || {};
    this.namespace.$hooks = namespace.$hooks || []; 
    this.namespace.$hooks = this.namespace.$hooks.concat(namespace.exportedHooks ? namespace.exportedHooks.map(({ type, fn }) => ({
      type,
      fn: unserialize$1(fn),
    })) : []);
    delete this.namespace.exportedHooks;
    this.init();
  }

  setDOMParent(...args) { return setDOMParent.call(this, this.namespace, ...args); }
  update(...args) { return update.call(this, this.namespace, ...args); }
  init(...args) { return init.call(this, this.namespace, ...args); }
  createDOMElement(...args) { return createDOMElement.call(this, this.namespace, ...args); }
  removeDOMElement(...args) { return removeDOMElement.call(this, this.namespace, ...args); }
  appendDOMElement(...args) { return appendDOMElement.call(this, this.namespace, ...args); }
  setAttributes(...args) { return setAttributes.call(this, this.namespace, ...args); }
  getAttributes() { return getAttributes.call(this, this.namespace); }
  getAttribute(attribute) { return getAttribute.call(this, this.namespace, attribute); }
  removeAttributes(...args) { return removeAttributes.call(this, this.namespace, ...args); }
  bindDOMEvents(...args) { return bindDOMEvents.call(this, this.namespace, ...args); }
  exportToJSON() { return exportToJSON.call(this, this.namespace) }
}

exports.DOMDriver = DOMDriver;
exports.appendDOMElement = appendDOMElement;
exports.bindDOMEvents = bindDOMEvents;
exports.createDOMElement = createDOMElement;
exports.getAttribute = getAttribute;
exports.getAttributes = getAttributes;
exports.init = init;
exports.removeAttributes = removeAttributes;
exports.removeDOMElement = removeDOMElement;
exports.setAttributes = setAttributes;
exports.setDOMParent = setDOMParent;
exports.update = update;
