'use strict';

var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

var pubsubExports = {};
var pubsub$1 = {
  get exports(){ return pubsubExports; },
  set exports(v){ pubsubExports = v; },
};

(function (module) {
	(function(scope) {
		var pubsubInstance = null;
		var pubsubConfig = null;

		if(typeof pubsub === 'object') {
			pubsubConfig = pubsub;
		//node.js config from global
		} else if(typeof commonjsGlobal === 'object' && typeof commonjsGlobal.pubsubConfig === 'object') {
			pubsubConfig = commonjsGlobal.pubsubConfig;
		}

		function Pubsub(config) {
			var _eventObject = {};
			var options = {
				separator : (config && config.separator) ? config.separator : '/',
				recurrent : (config && typeof config.recurrent === 'boolean') ? config.recurrent : (false),
				depth     : (config && typeof config.depth === 'number') ? config.depth : null,
				async     : (config && typeof config.async === 'boolean') ? config.async : (false),
				context   : (config && config.context) ? config.context : null,
				log       : (config && config.log) ? config.log : (false)
			};

			function forEach(dataArray, callback) {
				var i = 0,
					arrayLength = dataArray.length;

				for(i = 0; i < arrayLength; i++) {
					callback(i, dataArray[i]);
				}
			}

	        function isArray (obj) {
				return Array.isArray ? Array.isArray(obj) : Object.prototype.toString.call(obj) === '[object Array]';
	        }

	        function executeCallback(subscriptions, args, async) {
				async = (typeof async === 'boolean') ? async : options.async;
				if(!subscriptions.length) {
					return;
				}

				// clone array - callbacks can unsubscribe other subscriptions
				// reduces a lot performance but is safe
				var executedSubscriptions = subscriptions.slice();

				forEach(executedSubscriptions, function(subscriptionId, subscription) {
					if(typeof subscription === 'object' && executedSubscriptions.hasOwnProperty(subscriptionId)) {
						if(async) {
							setTimeout(function() {
								subscription.callback.apply(subscription.context, args);
							}, 4);
						} else {
							subscription.callback.apply(subscription.context, args);
						}
					}
				});
			}

			function executePublishWildcard(nsObject, args) {
				var nsElement;
				for(nsElement in nsObject) {
					if(nsElement[0] !== '_' && nsObject.hasOwnProperty(nsElement)) {
						executeCallback(nsObject[nsElement]._events, args);
					}
				}
			}

			function publish(nsObject, args, parts, params) {
				// work on copy - not on reference
				parts = parts.slice();

				var iPart = parts.shift();
				var depth = params.depth;
				var async = params.async;
				var partsLength = params.partsLength;
				var recurrent = params.recurrent;
				var partNumber = (partsLength - parts.length);

				// parts is empty
				if(!iPart) {
					executeCallback(nsObject._events, args, async);
					return;
				}
				// handle subscribe wildcard
				if(typeof nsObject['*'] !== 'undefined') {
					publish(nsObject['*'], args, parts, params);
				}

				// handle publish wildcard
				if(iPart === '*') {
					executePublishWildcard(nsObject, args);
				}

				// no namespace = leave publish
				if(typeof nsObject[iPart] === "undefined") {
					if(params.log) {
						console.warn('There is no ' + params.nsString + ' subscription');
					}
					return;
				}

				nsObject = nsObject[iPart];

				if(recurrent === true && typeof depth !== 'number') { //depth is not defined
					executeCallback(nsObject._events, args, async);
					if(parts.length === 0) {
						return;
					}
				} else if(recurrent === true && typeof depth === 'number' && partNumber >= (partsLength - depth)) { //if depth is defined
					executeCallback(nsObject._events, args, async);
				}

				publish(nsObject, args, parts, params);
			}

			function subscribe(nsString, callback, params) {
				var parts = nsString.split(options.separator),
					nsObject, //Namespace object to which we attach event
					context = (params && typeof params.context !== 'undefined') ? params.context : options.context,
					eventObject = null,
					i = 0;

				if(!context) {
					context = callback;
				}

				//Iterating through _eventObject to find proper nsObject
				nsObject = _eventObject;
				for(i = 0; i < parts.length; i += 1) {
					if(typeof nsObject[parts[i]] === "undefined") {
						nsObject[parts[i]] = {};
						nsObject[parts[i]]._events = [];
					}
					nsObject = nsObject[parts[i]];
				}

				eventObject = {
					callback	: callback,
					context		: context // "this" parameter in executed function
				};

				nsObject._events.push(eventObject);
				return { namespace : parts.join(options.separator),
					event : eventObject };
			}

			function unsubscribe(subscribeObject) {
				if(subscribeObject === null || typeof subscribeObject === 'undefined') {
					return null;
				}
				var nsString = subscribeObject.namespace,
					eventObject = subscribeObject.event,
					parts = nsString.split(options.separator),
					nsObject,
					i = 0;

				//Iterating through _eventObject to find proper nsObject
				nsObject = _eventObject;
				for(i = 0; i < parts.length; i += 1) {
					if(typeof nsObject[parts[i]] === "undefined") {
						if(options.log) {
							console.error('There is no ' + nsString + ' subscription');
						}
						return null;
					}
					nsObject = nsObject[parts[i]];
				}

				forEach(nsObject._events, function(eventId) {
					if(nsObject._events[eventId] === eventObject) {
						nsObject._events.splice(eventId, 1);
					}
				});
			}

			return {
				/**
				 * Publish event
				 * @param nsString string namespace string splited by dots
				 * @param args array of arguments given to callbacks
				 * @param params paramaters possible:
				 *        @param recurrent bool should execution be bubbled throught namespace
				 *        @param depth integer how many namespaces separated by dots will be executed
				 */
				publish : function(nsString, args, params) {
					var parts = nsString.split(options.separator),
						recurrent = (typeof params === 'object' && params.recurrent) ? params.recurrent : options.recurrent, // bubbles event throught namespace if true
						depth = (typeof params === 'object' && params.depth) ? params.depth : options.depth,
						async = (typeof params === 'object' && params.async) ? params.async : options.async,
						partsLength = parts.length;

					if(!parts.length) {
						if(options.log) {
							console.error('Wrong namespace provided ' + nsString);
						}
						return;
					}

					publish(_eventObject, args, parts, {
						recurrent : recurrent,
						depth : depth,
						async : async,
						parts : parts,
						nsString : nsString,
						partsLength : partsLength
					});
				},
				/**
				 * Subscribe event
				 * @param nsString string namespace string splited by dots
				 * @param callback function function executed after publishing event
				 * @param params given params
				 *		@param context object/nothing Optional object which will be used as "this" in callback
				 */
				subscribe : function(nsString, callback, params) {
					var self = this,
						subscriptions = [];

					// array of callbacks - multiple subscription
					if(isArray(callback)) {
						forEach(callback, function(number) {
							var oneCallback = callback[number];

							subscriptions =	subscriptions.concat(self.subscribe(nsString, oneCallback, params));
						});
					// array of namespaces - multiple subscription
					} else if(isArray(nsString)) {
						forEach(nsString, function(number) {
							var namespace = nsString[number];

							subscriptions =	subscriptions.concat(self.subscribe(namespace, callback, params));
						});
					} else {
						return subscribe.apply(self, arguments);
					}
					return subscriptions;
				},
				/**
				 * subscribeOnce event - subscribe once to some event, then unsubscribe immadiately
				 * @param nsString string namespace string splited by dots
				 * @param callback function function executed after publishing event
				 * @param params given params
				 *		@param context object/nothing Optional object which will be used as "this" in callback
				 */
				subscribeOnce : function(nsString, callback, params) {
					var self = this,
						subscription = null;

					function subscriptionCallback() {
						var context = this;

						callback.apply(context, arguments);
						self.unsubscribe(subscription);
					}

					subscription = self.subscribe(nsString, subscriptionCallback, params);
					return subscription;
				},
				/**
				 * Unsubscribe from given subscription
				 * @param subscribeObject subscription object given on subscribe (returned from subscription)
				 */
				unsubscribe : function(subscribeObject) {
					var self = this;

					//if we have array of callbacks - multiple subscription
					if(isArray(subscribeObject)) {
						forEach(subscribeObject, function(number) {
							var oneSubscribtion = subscribeObject[number];

							unsubscribe.apply(self, [oneSubscribtion]);
						});
					} else {
						unsubscribe.apply(self, arguments);
					}
				},
				/**
				 * newInstance - makes new instance of pubsub object with its own config
				 * @param params instance configuration
				 *        @param separator separator (default is "/")
				 *        @param recurrent should publish events be bubbled through namespace
				 *        @param async should publish events be asynchronous - not blocking function execution
				 *        @param log console.warn/error every problem
				 */
				newInstance : function(params) {
					return new Pubsub(params);
				}
			}; //return block
		}
		pubsubInstance = new Pubsub(pubsubConfig);

		//node.js
		if(module.exports) {
			module.exports = pubsubInstance;
		}

		if(typeof window === 'object') {
			window.pubsub = pubsubInstance;
			if(window !== scope) {
				scope.pubsub = pubsubInstance;
			}
		}
	})(commonjsGlobal);
} (pubsub$1));

var PubSub = pubsubExports;

const COMMENT_REG_EXP$1 = new RegExp(/\n+|(\/\*\*\/\n)+/, 'g');
const NEW_LINE_REG_EXP$1 = new RegExp(/^(\n+|\t+|\t\n+)(?!\w)$/, 'gm');
const BACKTICK_REG_EXP$1 = new RegExp(/`/, 'gm');

const serialize = (funct) => {
  if (!(funct instanceof Function)) throw new TypeError('serialize: argument should be a function');
  const txt = funct.toString();
  const args = txt.slice(txt.indexOf('(') + 1, txt.indexOf(')')).split(',');
  const body = txt.slice(txt.indexOf('{') + 1, txt.lastIndexOf('}'));
  return {
    args: args.map(el => el.replace(COMMENT_REG_EXP$1, '')
      .replace(NEW_LINE_REG_EXP$1, '')
      .replace(BACKTICK_REG_EXP$1, '')),
    body,
  };
};

var serialize$1 = serialize;

const COMMENT_REG_EXP = new RegExp(/\n+|(\/\*\*\/\n)+/, 'g');
const NEW_LINE_REG_EXP = new RegExp(/^(\n+|\t+|\t\n+)(?!\w)$/, 'gm');
const BACKTICK_REG_EXP = new RegExp(/`/, 'gm');
const COMMENTS_AND_BACKTICK = new RegExp(/(\/\*``\*\/)+/, 'g');

var unserialize = (serialized) => {
  const args = serialized.args.map(el => el.replace(COMMENT_REG_EXP, '').replace(COMMENTS_AND_BACKTICK, '').replace(NEW_LINE_REG_EXP, '').replace(BACKTICK_REG_EXP, ''));
  const { body } = serialized;
  let func = new Function();
  try {
    func = new Function(args, body);
  } catch (e) {
    console.error(e);
  }
  return func;
};

var unserialize$1 = unserialize;

const createTopic = (namespace, topicName) => {
  if (namespace.topics[topicName]) return
  namespace.topics[topicName] = {
    name: topicName,
    subscribers: [],
  };
};

const subscribe = function (namespace, topicName, fn, options) {
  if (typeof fn !== 'function') throw new TypeError(`Cannot subscribe to ${topicName}. Provide a Function`);
  if (!namespace.topics[topicName]) createTopic(namespace, topicName);
  const token =  PubSub.subscribe(namespace.topics[topicName].name, fn, options);
  namespace.topics[topicName].subscribers.push({
    token,
    fn,
    options,
  });
  return token
};

const unsubscribe = function (namespace, token) {
  Object.values(namespace.topics).find(t => {
    const sb = t.subscribers.find(s => s.token === token);
    if (sb) {
      const sbIndex = t.subscribers.findIndex(s => s.token === token);
      if (sbIndex >= 0) t.subscribers.splice(sbIndex, 1);
      return sb;
    } else {
      return
    }
  });
  return PubSub.unsubscribe(token);
};

const publish = function (namespace, topicName, ...args) {
  return PubSub.publish(topicName, [...args]);
};

/**
 * 
 * Driver update method
 * trigger the update cycle
 * 
 * @param {Object} namespace 
 * @param {Object} mo 
 */
const update = function update(namespace, ...args) {
  const parent = this;
  if (namespace.$hooks) namespace.$hooks.filter(h => h.type === 'update').forEach(h => h.fn.apply(parent, [namespace, ...args]));
};

const exportToJSON = ({
  topics,
  $hooks,
}) => ({
  exportedTopics: Object.keys(topics).reduce((acc, key) => {
    const topic = {
      name: key,
      subscribers: topics[key].subscribers.map(m => serialize$1(m.fn)),
    };
    acc.push(topic);
    return acc;
  }, []),
  exportedHooks: $hooks.map(({ type, fn }) => ({
    type,
    fn: serialize$1(fn),
  })),
});

class EventsDriver {
  constructor(namespace) {
    this.id = 'Events';
    this.namespace = namespace;
    this.namespace.topics = this.namespace.topics || {};
    this.namespace.$hooks = this.namespace.$hooks || []; 
    this.namespace.$hooks = this.namespace.$hooks.concat(namespace.exportedHooks ? namespace.exportedHooks.map(({ type, fn }) => ({
      type,
      fn: unserialize$1(fn),
    })) : []);
    delete this.namespace.exportedHooks;
    if (this.namespace.exportedTopics) {
      this.namespace.exportedTopics.forEach((topic) => {
        this.createTopic(topic.name);
        topic.subscribers.forEach(sub => this.subscribe(topic.name, unserialize$1(sub)));
      });
      delete this.namespace.exportedTopics;
    }
  }
  createTopic(topicName) { return createTopic.call(this, this.namespace, topicName); }
  subscribe(topicName, fn, options) { return subscribe.call(this, this.namespace, topicName, fn, options); }
  unsubscribe(token) { return unsubscribe.call(this, this.namespace, token); }
  publish(topicName, ...args) { return publish.call(this, this.namespace, topicName, ...args); }
  update(mo) { return update.call(this, this.namespace, mo); }
  exportToJSON() { return exportToJSON.call(this, this.namespace); }
}

exports.EventsDriver = EventsDriver;
exports.createTopic = createTopic;
exports.publish = publish;
exports.subscribe = subscribe;
exports.unsubscribe = unsubscribe;
exports.update = update;
