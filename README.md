# MODrivers

## Dependencies

- [PubSubJS](https://sahadar.github.io/pubsub/#pubsub-js)

## Dev-Dependencies

- [rollup](https://rollupjs.org/)
  - [plugin commonjs](https://github.com/rollup/plugins/tree/master/packages/commonjs)
  - [plugin node resolve](https://github.com/rollup/plugins/tree/master/packages/node-resolve)
- [globby](https://www.npmjs.com/package/globby)